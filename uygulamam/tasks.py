# -*- coding: utf-8 -*-

from __future__ import absolute_import

from celery import shared_task


@shared_task
def testet ():
    import time



    ### Burada sayfa yüklendse bile yapılması istenilen işlemler sıralanacak.
    ### Ziyaretçi sayfayı yüklenmiş görecek ancak arka planda rabbitmq ve celery işlemleri yapmaya devam edecek.

    time.sleep (5)
    return "OK"
