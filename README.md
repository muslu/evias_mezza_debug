Evias

Mezzanine & Django Debug Toolbar



* http://mezzanine.jupo.org
* http://django-debug-toolbar.readthedocs.org/en/1.4/configuration.html


Asenkron zamanlama ile çalışma için celery ve rabbitmq kullanılmıştır.

kısaca; 

rabbitmq arka planda çalışan bir servis. mesaj alma ve cevap verme özelliklerine sahiptir.


http://www.rabbitmq.com/documentation.html

sudo apt-get install rabbitmq-server
service rabbitmq-server start

celery;
bizim yerimize yapmak istediğimiz işlemleri sıraya koyar ve rabbitmq ile konuşarak işlemleri kontrol edip bilgi verir.


http://docs.celeryproject.org/en/latest/userguide/index.html

pip install celery
muslu@muslu-MS-7641:~/mezza/eviasmezza$ celery -A eviasmezza worker -l info

Aşağıdaki örnekteki gibi ana sayfa yüklendiğinde testet taskı göreve başlar ama return değeri var diye sayfayı yükler ancak 5 sn sonra task görevi tamamlar ve OK mesajını gösterir.

[2016-02-23 09:52:19,002: INFO/MainProcess] Received task: uygulamam.tasks.testet[865ec4de-93cc-432a-8525-0d4cb59e2fdc]
[2016-02-23 09:52:24,011: INFO/MainProcess] Task uygulamam.tasks.testet[865ec4de-93cc-432a-8525-0d4cb59e2fdc] succeeded in 5.006007685s: 'OK'